﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace курсова_проби
{
    internal class UserFileService
    {

        private const string fileName = "users.csv";

        public void save(User user)
        {
            string[] strings = System.IO.File.ReadAllLines(fileName);
            int newUserNumber = 1;
            
            if (strings.Length > 0)
            {
                string userNumber = strings[strings.Length - 1].Split(',')[0];
                newUserNumber = Int32.Parse(userNumber) + 1;
            }

            string line = "" + newUserNumber;
            File.AppendAllText(fileName, string.Format("{0}{1},{2}", Environment.NewLine, line, user));
        }

        public List<User> GetAllUsers()
        {
            string[] strings = System.IO.File.ReadAllLines(fileName);
            List<User> result = new List<User>();
            for(int i = 0; i < strings.Length; i++)
            {
                result.Add(strings[i]);
            }
            return result;
        }

    }
}
