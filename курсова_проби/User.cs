﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace курсова_проби
{
    internal class User
    {
        private string name;
        private string lastname;
        private string nickname;
        private float weight;
        private int jerk;
        private int snatch;
        private int deadlift;
        private int snatchDeadLift;
        private int backSquat;
        private int frontSquat;
        private int standingPress;

        public string Name
            { get { return name; } set { this.name = value.Trim(); } }
        public string Lastname
            { get { return lastname; } set { this.lastname = value; } }
        public string Nickname
            { get { return nickname; } set { this.nickname = value.Trim(); } }
        public float Weight
            { get { return weight; } set { this.weight = value; } }
        public int Jerk
            { get { return jerk; } set { this.jerk = value; } }
        public int Snatch
            { get { return snatch; } set { this.snatch = value; } }
        public int Deadlift
            { get { return deadlift; } set { this.deadlift = value; } }
        public int BackSquat
            { get { return backSquat; } set { this.backSquat = value; } }
        public int SnatchDeadLift
            { get { return snatchDeadLift; } set { this.snatchDeadLift = value; } }
        public int FrontSquat
            { get { return frontSquat; } set { this.frontSquat = value; } }
        public int StandingPress
            { get { return standingPress; } set { this.standingPress = value; } }

        
        public override string ToString()
        {
            string result = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",
                name,
                lastname,
                nickname,
                weight,
                snatch,
                jerk,
                deadlift,
                snatchDeadLift,
                backSquat,
                frontSquat,
                standingPress
                );
            return result;
        }
    }
}
