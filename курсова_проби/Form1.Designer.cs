﻿namespace курсова_проби
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxUsername = new System.Windows.Forms.TextBox();
            this.txtBoxNikname = new System.Windows.Forms.TextBox();
            this.txtBoxSnatch = new System.Windows.Forms.TextBox();
            this.txtBoxBodyWeight = new System.Windows.Forms.TextBox();
            this.textBoxJerk = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtBoxLastName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSnatchDL = new System.Windows.Forms.TextBox();
            this.txtBoxDL = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBoxFrontSquat = new System.Windows.Forms.TextBox();
            this.txtBoxBackSquat = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBoxStandingPress = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(127, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Реєстрація";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(54, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ім\'я";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Нікнейм";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Вага";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(150, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "Рекорди";
            // 
            // txtBoxUsername
            // 
            this.txtBoxUsername.Location = new System.Drawing.Point(57, 80);
            this.txtBoxUsername.Name = "txtBoxUsername";
            this.txtBoxUsername.Size = new System.Drawing.Size(100, 22);
            this.txtBoxUsername.TabIndex = 5;
            this.txtBoxUsername.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtBoxNikname
            // 
            this.txtBoxNikname.Location = new System.Drawing.Point(57, 136);
            this.txtBoxNikname.Name = "txtBoxNikname";
            this.txtBoxNikname.Size = new System.Drawing.Size(100, 22);
            this.txtBoxNikname.TabIndex = 6;
            // 
            // txtBoxSnatch
            // 
            this.txtBoxSnatch.Location = new System.Drawing.Point(247, 232);
            this.txtBoxSnatch.Name = "txtBoxSnatch";
            this.txtBoxSnatch.Size = new System.Drawing.Size(40, 22);
            this.txtBoxSnatch.TabIndex = 8;
            this.txtBoxSnatch.WordWrap = false;
            this.txtBoxSnatch.TextChanged += new System.EventHandler(this.txtBoxSnatch_TextChanged);
            // 
            // txtBoxBodyWeight
            // 
            this.txtBoxBodyWeight.Location = new System.Drawing.Point(218, 136);
            this.txtBoxBodyWeight.Name = "txtBoxBodyWeight";
            this.txtBoxBodyWeight.Size = new System.Drawing.Size(100, 22);
            this.txtBoxBodyWeight.TabIndex = 7;
            // 
            // textBoxJerk
            // 
            this.textBoxJerk.Location = new System.Drawing.Point(247, 260);
            this.textBoxJerk.Name = "textBoxJerk";
            this.textBoxJerk.Size = new System.Drawing.Size(40, 22);
            this.textBoxJerk.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(21, 494);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(149, 53);
            this.button1.TabIndex = 11;
            this.button1.Text = "Зареєструватись";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtBoxLastName
            // 
            this.txtBoxLastName.Location = new System.Drawing.Point(218, 80);
            this.txtBoxLastName.Name = "txtBoxLastName";
            this.txtBoxLastName.Size = new System.Drawing.Size(100, 22);
            this.txtBoxLastName.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.Location = new System.Drawing.Point(215, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Фамілія";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(75, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Ривок";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(75, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Поштовх";
            // 
            // txtSnatchDL
            // 
            this.txtSnatchDL.Location = new System.Drawing.Point(247, 316);
            this.txtSnatchDL.Name = "txtSnatchDL";
            this.txtSnatchDL.Size = new System.Drawing.Size(40, 22);
            this.txtSnatchDL.TabIndex = 18;
            // 
            // txtBoxDL
            // 
            this.txtBoxDL.Location = new System.Drawing.Point(247, 288);
            this.txtBoxDL.Name = "txtBoxDL";
            this.txtBoxDL.Size = new System.Drawing.Size(40, 22);
            this.txtBoxDL.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(75, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "Тяга ривкова";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(75, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 16);
            this.label10.TabIndex = 19;
            this.label10.Text = "Тяга";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(74, 376);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 16);
            this.label11.TabIndex = 24;
            this.label11.Text = "Присідання на груді";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(75, 348);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 16);
            this.label12.TabIndex = 23;
            this.label12.Text = "Присідання на спині";
            // 
            // txtBoxFrontSquat
            // 
            this.txtBoxFrontSquat.Location = new System.Drawing.Point(247, 373);
            this.txtBoxFrontSquat.Name = "txtBoxFrontSquat";
            this.txtBoxFrontSquat.Size = new System.Drawing.Size(40, 22);
            this.txtBoxFrontSquat.TabIndex = 22;
            // 
            // txtBoxBackSquat
            // 
            this.txtBoxBackSquat.Location = new System.Drawing.Point(247, 345);
            this.txtBoxBackSquat.Name = "txtBoxBackSquat";
            this.txtBoxBackSquat.Size = new System.Drawing.Size(40, 22);
            this.txtBoxBackSquat.TabIndex = 21;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(201, 494);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(149, 53);
            this.button2.TabIndex = 25;
            this.button2.Text = "Увійти до існучого";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(75, 403);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 16);
            this.label13.TabIndex = 26;
            this.label13.Text = "Жим стоячи";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // txtBoxStandingPress
            // 
            this.txtBoxStandingPress.Location = new System.Drawing.Point(247, 401);
            this.txtBoxStandingPress.Name = "txtBoxStandingPress";
            this.txtBoxStandingPress.Size = new System.Drawing.Size(40, 22);
            this.txtBoxStandingPress.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 571);
            this.Controls.Add(this.txtBoxStandingPress);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtBoxFrontSquat);
            this.Controls.Add(this.txtBoxBackSquat);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtSnatchDL);
            this.Controls.Add(this.txtBoxDL);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBoxLastName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxJerk);
            this.Controls.Add(this.txtBoxSnatch);
            this.Controls.Add(this.txtBoxBodyWeight);
            this.Controls.Add(this.txtBoxNikname);
            this.Controls.Add(this.txtBoxUsername);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxUsername;
        private System.Windows.Forms.TextBox txtBoxNikname;
        private System.Windows.Forms.TextBox txtBoxSnatch;
        private System.Windows.Forms.TextBox txtBoxBodyWeight;
        private System.Windows.Forms.TextBox textBoxJerk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtBoxLastName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSnatchDL;
        private System.Windows.Forms.TextBox txtBoxDL;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBoxFrontSquat;
        private System.Windows.Forms.TextBox txtBoxBackSquat;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBoxStandingPress;
    }
}

